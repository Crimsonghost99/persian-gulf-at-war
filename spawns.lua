local objective_names
local extra_objective_names = {"Annihilation","Argument","Blind Knuckle","Blockade","Brass Ghost","Camel",
                                "Cartwheel","Charlie","Crossword","Flintlock","Full Steam","India","Javelin",
                                "Leviathan","Machete","Monsoon Rain","Morass","Ocean Obelisk","Oscar",
                                "Pre-Release","Purple Dust","Purple Ghost","Purple Knife","Purple Truth",
                                "Python","Red Snow","Sourdough","Vampire","Yellow Lilly","Abracadabra",
                                "Anvil","Blind Gate","Brannigan","Bronze Citadel","Bronze Shark","Bronze Tornado",
                                "Buffoon","Chronicle","Crackpot","Desperado","Disenchant","Epiphany","Feature Length",
                                "Fire Fighter","Gemini","Good Fortune","Harlequin","Homesick","Horseshoe","Mastermind",
                                "Mesmerize","Oak Tree","Obelisk","Ocean Whirlwind","Predator","Resurrection",
                                "Sea Charger","Silver Champion","Silver Doom","Steamroller","Tango","Taurus",
                                "Twiligt Zone","Urban Gate","Urban Paladin","Voodoo Vibes","Water Nymph","White Truth",
                                "Yellow Phantom"}
local objectiveFile = io.open(lfs.writedir() .. "Scripts\\objectives.json", 'r')
if (objectiveFile) then
    local objective_names_reader = objectiveFile:read("*all")
    objectiveFile:close()
    objective_names = json:decode(objective_names_reader)
else
    objective_names = extra_objective_names
end
if (#objective_names <= 15) then
    -- Just add a few more in case supporters haven't been entering theirs.
    for _, obj in ipairs(extra_objective_names) do
        table.insert(objective_names, obj)
        if (#objective_names > 30) then
            break
        end
    end
end
objective_names = shuffle(objective_names)

objective_idx = 1

getMarkerId = function()
    objectiveCounter = objectiveCounter + 1
    return objectiveCounter
end

getCallsign = function()
    local callsign = objective_names[objective_idx]
    objective_idx = objective_idx + 1
    if objective_idx > #objective_names then
        log("Not enough objectives. Circling around to 1 again")
        objective_idx = 1
    end
    return callsign
end


function respawnHAWKFromState(_points)
    log("Spawning hawk from state")
    -- spawn HAWK crates around center point
    ctld.spawnCrateAtPoint("blue",547, _points["Hawk pcp"])
    ctld.spawnCrateAtPoint("blue",540, _points["Hawk ln"])
    ctld.spawnCrateAtPoint("blue",545, _points["Hawk sr"])
    ctld.spawnCrateAtPoint("blue",546, _points["Hawk tr"])
    ctld.spawnCrateAtPoint("blue",548, _points["Hawk cwar"])

    -- spawn a helper unit that will "build" the site
    local _SpawnObject = Spawner( "HawkHelo" )
    local _SpawnGroup = _SpawnObject:SpawnAtPoint({x=_points["Hawk pcp"]["x"], y=_points["Hawk pcp"]["z"]})
    local _unit=_SpawnGroup:getUnit(1)

    -- enumerate nearby crates
    local _crates = ctld.getCratesAndDistance(_unit)
    local _crate = ctld.getClosestCrate(_unit, _crates)
    local terlaaTemplate = ctld.getAATemplate(_crate.details.unit)

    ctld.unpackAASystem(_unit, _crate, _crates, terlaaTemplate)
    _SpawnGroup:destroy()
    log("Done Spawning hawk from state")
end


function respawnNASAMSFromState(_points)
    log("Spawning NASAMS from state")
    -- spawn NASAM crates around center point
    ctld.spawnCrateAtPoint("blue",543, _points["NASAMS_Command_Post"])
    ctld.spawnCrateAtPoint("blue",544, _points["NASAMS_Radar_MPQ64F1"])
    ctld.spawnCrateAtPoint("blue",541, _points["NASAMS_LN_C"])

    -- spawn a helper unit that will "build" the site
    local _SpawnObject = Spawner( "HawkHelo" )
    local _SpawnGroup = _SpawnObject:SpawnAtPoint({x=_points["NASAMS_Command_Post"]["x"], y=_points["NASAMS_Command_Post"]["z"]})
    local _unit=_SpawnGroup:getUnit(1)

    -- enumerate nearby crates
    local _crates = ctld.getCratesAndDistance(_unit)
    local _crate = ctld.getClosestCrate(_unit, _crates)
    local terlaaTemplate = ctld.getAATemplate(_crate.details.unit)

    ctld.unpackAASystem(_unit, _crate, _crates, terlaaTemplate)
    _SpawnGroup:destroy()
    log("Done Spawning NASAMS from state")
end

ShipGroupsTrackedByState = {
    "Pyotr", "Yulin", "Wuhan",                      --Pyotr Battle Group
    "Haikou", "Neustrashimy",                       --Bandar Abbas Naval Base
    "AbuMusa Neustrashimy",                         --Abu Mussa Defense
    "Moskva", "Neustrashimy2", "Guangzhou",         --Moskva Battle Group
}

SA6SpawnZones = {
    "NorthSA6Zone1",
    "NorthSA6Zone2",
    "NorthSA6Zone3",
    "NorthSA6Zone4",
    "NorthSA6Zone5",
    "NorthSA6Zone6",
    "NorthSA6Zone7",
    "NorthSA6Zone8",
    "NorthSA6Zone9",
    "NorthSA6Zone10",
    "NorthSA6Zone11",
    "NorthSA6Zone12",
    "NorthSA6Zone13",
    "NorthSA6Zone14"
}

SA10SpawnZones = {
    "NorthSA10Zone1",
    "NorthSA10Zone2",
    "NorthSA10Zone3",
    "NorthSA10Zone4",
    "NorthSA10Zone5",
    "NorthSA10Zone6",
    "NorthSA10Zone7",
    "NorthSA10Zone8"
}

zone_parents = {
    ["Al Ain Intl"] = {"NorthStatic3"},
    ["FARP ALPHA"] = {"NorthStatic1", "NorthStatic2"},
    ["Al Maktoum Intl"] = {"NorthSA6Zone6", "NorthSA6Zone7"},
    ["Al Minhad AFB"] = {"NorthStatic4","NorthStatic5"},
    ["Dubai Intl"] = {"NorthStatic6", "NorthStatic7","NorthSA6Zone4","NorthSA6Zone1","NorthSA6Zone3"},
    ["Fujairah Intl"] = {"NorthSA6Zone8","NorthSA6Zone5"},
    ["Sharjah Intl"] = {"NorthStatic8", "NorthStatic9", "NorthSA6Zone2", "NorthSA6Zone9", "NorthSA6Zone10"},
    ["Ras Al Khaimah Intl"] = {"NorthStatic10","NorthStatic11","NorthSA6Zone14"},
    ["FARP Charlie"] = {"NorthStatic12","NorthSA6Zone13","NorthSA6Zone12","NorthSA6Zone15"},
    ["Khasab"] = {"NorthStatic14","NorthStatic13","NorthSA6Zone16","NorthSA6Zone17","NorthSA6Zone18","NorthSA6Zone19","NorthSA6Zone20"},
    ["Qeshm Island"] = {"NorthStatic15","NorthStatic16","NorthStatic17","NorthSA10Zone1","NorthSA10Zone2","NorthSA10Zone4", "NorthSA6Zone21", "NorthSA6Zone22", "NorthSA6Zone23"},
    ["FARP Qeshm"] = {"NorthSA10Zone7","NorthSA10Zone5", "NorthSA6Zone24", "NorthSA6Zone25"},
    ["Havadarya"] = {"NorthSA10Zone6", "NorthSA10Zone11", "NorthSA10Zone13", "NorthSA10Zone14"}
}

ALLIES = coalition.side.BLUE
eligibleSpawnZones = function()
    local eligibleZones = {}
    for ab,zones in pairs(zone_parents) do
        local airbase = Airbase.getByName(ab)
        --Not gonna bother with a nilcheck here. It's internal data above and should be correct.
        local coalition = airbase:getCoalition()
        if coalition ~= ALLIES then
            log("ELIGIBLEZONES -- ab ["..ab.."] was not allied! including.")
            for _,z in pairs(zones) do table.insert(eligibleZones, z) end
        end
    end
    return eligibleZones
end
searchEligibleZones = function(searchWord)
    local zones = eligibleSpawnZones()
    local foundZones = {}
    for _,zone in pairs(zones) do
        if string.match(zone, searchWord) then
            log("ELIGIBLEZONES -- Zonename ["..zone.."] matched search for ["..searchWord.."]. Including.")
            table.insert(foundZones, zone)
        end
    end
    return foundZones
end

eligibleSA10Zones = function() return searchEligibleZones("SA10") end
eligibleSA6Zones = function() return searchEligibleZones("SA6") end

getSA10Zone = function()
    local zone = randomchoice(eligibleSA10Zones())
    if zone ~= nil then
        log("ELIGIBLEZONES -- Got SA10 Zone -- ".. zone)
    else
        log("ELIGIBLEZONES -- No More SA10 zones")
    end
    return zone
end
getSA6Zone = function()
    local zone = randomchoice(eligibleSA6Zones())
    if zone ~= nil then
        log("ELIGIBLEZONES -- Got SA6 Zone -- ".. zone)
    else
        log("ELIGIBLEZONES -- No More SA6 zones")
    end
    return zone
end
-- validate_zone_parents = function()
--     local problems = 0
--     for ab,zones in pairs(zone_parents) do
--         local airbase = Airbase.getByName(ab)
--         if airbase == nil then
--             trigger.action.outText("PROBLEM WITH GETTING AIRFIELD: " .. ab, 10)
--             problems = problems + 1
--         else
--             for _,zone in pairs(zones) do
--                 local z = trigger.misc.getZone(zone)
--                 if z == nil then
--                     trigger.action.outText("PROBLEM WITH GETTING ZONE ["..zone.."] UNDER AIRFIELD ["..ab.."]", 10)
--                     problems = problems + 1
--                 end
--             end
--         end
--     end
--     trigger.action.outText("Zone validation complete", 10)
-- end
-- validate_zone_parents()

-- Tries to get a free sam zone that has no other red units in it.
getFreeZone = function(zoneFunc)
    for i = 1, 5 do
        local zone = zoneFunc()
        local unitCount = getRedUnitCountInZone(zone)
        log("Checking zone ["..zone .."] for red unit count.")
        if unitCount == 0 then return zone end
        log("Zone ["..zone .."] had ["..unitCount.."] red units present. Trying to find another zone.")
    end
    -- Couldn't find a red-unit-free zone in 5 attempts. Give em back the next one.
    local zone = zoneFunc()
    log("Couldn't find a red-unit-free zone after 5 attempts. Returning zone ["..zone.."].")
    return zone
end

local logispawn = {
    type = "HEMTT TFFT",
    country = "USA",
    category = "Ground vehicles"
}

local function oil_rig_spawn()
    return function(point)
        return {
            type = "Oil platform",
            category = "Structures",
            country = "Russia",
            x = point.x,
            y = point.y
        }
    end
end


local function generate_oil_rig_spawns(count, origin_point)
    local spawns = {}
    for c=1,count do
        local offset = randomOffset(500,1000)
        local point =  {
            x = origin_point.x + offset.x,
            y = origin_point.y + offset.y
        }
        local spawn = oil_rig_spawn()(point)
        table.insert(spawns, spawn)
    end
    return spawns
end

local oil_rig_locations = {
    { x = 84346, y = 61361 },
    { x = 82169, y = 11836 },
    { x = 42724, y = -89221 },
    { x = -58264, y = 8246 },
    { x = -42134, y = 35646 },
    { x = -112073, y = 20539},
    { x = -20213, y = -168905},
    { x = -6346, y = -138039},
}
distanceToStatic = function(pt, static)
    local static_loc = mist.utils.makeVec2(static:getPosition().p)
    return mist.utils.get2DDist(static_loc, pt)
end

local tooCloseToOtherStatics = function(pt)
    for _, data in pairs(game_state["NavalStrike"]) do
        local statics = data["statics"]
        if statics ~= nil then
            local oil_rig_name = statics[1].name
            local static = StaticObject.getByName(oil_rig_name)
            local distance = distanceToStatic(pt, static)
            if static and distance <= 10000 then
                return true
            end
        end
    end
    return false
end

local find_free_oil_rig_location = function()
    for _,pt in pairs(shuffle(oil_rig_locations)) do
        -- Check all the current naval strikes for proximity!
        if not tooCloseToOtherStatics(pt) then
            return pt
        end
    end
    return randomFromList(oil_rig_locations)
end

get_oil_rig_location = function()
    local point = find_free_oil_rig_location()
    -- If it's still nil then we didn't find a spot. Probably nothing existing yet.
    return mist.utils.deepCopy({
        x = point.x + math.random(-3050, 3050),
        y = point.y + math.random(-3050, 3050)
    })
end

SpawnOilRigGroup = function(pt)
    local spawns = generate_oil_rig_spawns(math.random(1,3), pt)
    local completed = {}
    for _,s in pairs(spawns) do
        local added = mist.dynAddStatic(s)
        table.insert(completed, added)
    end
    return completed
end
-- Transport Spawns
NorthGeorgiaTransportSpawns = {}
local available_airports = {"Abu Dhabi Intl", "Al Ain Intl", "Al Maktoum Intl", "Al Minhad AFB", "Dubai Intl",
"Sharjah Intl", "Khasab", "Qeshm Island", "Al-Bateen", "Sas Al Nakheel", "Fujairah Intl"}
for i,abname in ipairs(available_airports) do
    NorthGeorgiaTransportSpawns[abname] = {Spawner(abname .. " Transport"), Spawner(abname .. " Transport Helo")}
end

NorthGeorgiaFARPTransportSpawns = {}
local available_farps = {"FARP Alpha", "FARP Bravo", "FARP Charlie", "FARP Delta", "FARP Qeshm"}
for i,abname in ipairs(available_farps) do
    NorthGeorgiaFARPTransportSpawns[abname] = {Spawner(abname .. " Transport"), nil, nil}
end

-- Tells a tanker (or other aircraft for that matter) to orbit between two points at a set altitude

WeatherPositioning = {}
WeatherPositioning.hMaxFlightAlt	= 5486	-- meters [18 000']: Don't let the aircraft fly higher than this as Hogs won't be able to refuel. TODO: Make overrideable
WeatherPositioning.vSpeed			= 160		-- m/s [300kts]: Default speed of the unit
WeatherPositioning.hClearance		= 305		-- meters [1000']: Default clearance to deconflict
-- Realistically if we use this to deconflict tankers as well, we'd need 2000' as chicks approach them 1000' low
WeatherPositioning.stdDensity = 1.225	-- kg/m^3
WeatherPositioning.stdPressure_inHg = 29.92	-- inHg so that we can use the feet calculations
WeatherPositioning.stdTemperature = 15	-- Celsius


-- Uses the aviation-level approximations for getting the pressure and density altitudes, except we use a slightly more accurate 1inHg/1200' linear approximation for presure lapse rate
-- Between 10-18000' this conversion is accurate to +-5knots
function WeatherPositioning.IAStoTAS(IASIn, altitude)

	local weather = env.mission.weather

	local seaLevelTemp	= weather.season.temperature
	local qnh			= weather.qnh / 25.4	-- Convert mmHg to inHg

	env.info("Sea Level Temperature: " .. seaLevelTemp)
	env.info("QNH: " .. qnh)

	-- Calculate Pressure Height
	local altFeet		= altitude * 3.28084										-- meters to feet
	local dQ			= qnh - WeatherPositioning.stdPressure_inHg
	local pressureAlt	= altFeet - dQ * 1200										-- 1200' for every inHg
	local tempDelta		= seaLevelTemp - WeatherPositioning.stdTemperature			-- DCS appears to model variation in temperature over height as the standard (-2 C per 1000' up to 36000'), so we can just get the ISA variation at sea level
	local densityAlt	= pressureAlt + 120 * tempDelta								-- 120' for every C above ISA

	local tempAlt		= WeatherPositioning.stdTemperature - (densityAlt / 500)							-- Get the approximated temperature at the input altitude (-2 C per 1000' up to 36000')
	local tempAbsAlt	= 273.15 + tempAlt											-- Convert temperature to Kelvin
	local absPressure	= 101325 * (WeatherPositioning.stdPressure_inHg - (densityAlt / 1200)) / WeatherPositioning.stdPressure_inHg	-- Calculate Absolute pressure at the given density alt
	local altDensity	= absPressure * 0.0289644 / (8.31447 * tempAbsAlt)			-- density = pressure * Molar mass / (Ideal Gas Constant * Absolute Temperature) [p = pM/RT]

	return IASIn * math.sqrt(WeatherPositioning.stdDensity / altDensity)
end

-- Generic altitude deconfliction
function WeatherPositioning.deconflictAltitude(altitudeIn, deconflictBase, deconflictTop)
    local hAltitude = altitudeIn
    if deconflictTop >= (altitudeIn - WeatherPositioning.hClearance) then
        hAltitude = math.min(deconflictBase - WeatherPositioning.hClearance, altitudeIn)
    end
    return hAltitude
end

function WeatherPositioning.avoidCloudLayer(planeGroup, MEGroupName, vSpeedIAS_ms)
    vSpeedIAS_ms = vSpeedIAS_ms or WeatherPositioning.vSpeed

    -- Calculate orbit height. Ignore for partial cloud conditions
    local clouds = env.mission.weather.clouds
    local cloudBase = clouds.base
    local cloudTop = clouds.base + clouds.thickness
    local hOrbit = WeatherPositioning.hMaxFlightAlt

    if clouds.density >= 7 then
        hOrbit = WeatherPositioning.deconflictAltitude(hOrbit, cloudBase, cloudTop)
    end

	-- Adjust input IAS into a TAS for the given altitude
	local vSpeedTAS = WeatherPositioning.IAStoTAS(vSpeedIAS_ms, hOrbit)

    local curRoute = mist.getGroupRoute(MEGroupName, true)

    for i = 1, #curRoute do
        curRoute[i].alt = hOrbit
        curRoute[i].speed = vSpeedTAS

        -- Modify any orbit taskings
        if #curRoute[i] ~= nil
            and #curRoute[i].task ~= nil
            and #curRoute[i].task.params ~= nil
            and #curRoute[i].task.params.tasks ~= nil then

            for _, curTask in pairs(curRoute[i].task.params.tasks) do
                if curTask.id == "Orbit" then
                    curTask.params.altitude = hOrbit
                    curTask.params.speed = vSpeedTAS
                end
            end
        end
    end
    log("Setting Orbit Height of ['"..planeGroup:getName().."' based on '"..MEGroupName.."'] to " .. hOrbit .."m @ " .. vSpeedTAS .. "m/s")
    return mist.goRoute(planeGroup, curRoute)
end


scheduledSpawns = {}
DestructibleStatics = {}
DestroyedStatics = {}
BlueSecurityForcesGroups = {}
BlueFarpSupportGroups = {}
-- Support Spawn
TexacoSpawn = Spawner("Texaco")
TexacoSpawn:OnSpawnGroup(function(grp)
    scheduledSpawns[grp:getUnit(1):getName()] = {TexacoSpawn, 600}
    WeatherPositioning.avoidCloudLayer(grp, TexacoSpawn.MEName, 129) -- Init against cloud base at 129 m/s (250 knots)
    push_tanker(grp:getName())
end)

ShellSpawn = Spawner("Shell")
ShellSpawn:OnSpawnGroup(function(grp)
    scheduledSpawns[grp:getUnit(1):getName()] = {ShellSpawn, 600}
    WeatherPositioning.avoidCloudLayer(grp, ShellSpawn.MEName, 144) -- Init against cloud base at 144m/s (280 knots)
    push_tanker(grp:getName())
end)

ArcoSpawn = Spawner("Arco")
ArcoSpawn:OnSpawnGroup(function(grp)
    scheduledSpawns[grp:getUnit(1):getName()] = {ArcoSpawn, 600}
    WeatherPositioning.avoidCloudLayer(grp, ArcoSpawn.MEName, 144) -- Init against cloud base at 144m/s (280 knots)
    -- Don't push. Arco is staying by the carrier.
end)

OverlordSpawn = Spawner("AWACS")
OverlordSpawn:OnSpawnGroup(function(grp)
    scheduledSpawns[grp:getUnit(1):getName()] = {OverlordSpawn, 600}
end)

CagAWACSSpawn = Spawner("AWACS CAG")
CagAWACSSpawn:OnSpawnGroup(function(grp)
    scheduledSpawns[grp:getUnit(1):getName()] = {CagAWACSSpawn, 600}
end)

-- Local defense spawns.  Usually used after a transport spawn lands somewhere.
AirfieldDefense = Spawner("AirfieldDefense")

-- Strategic REDFOR spawns
RussianTheaterSA10Spawn = { Spawner("SA10"), "SA10" }
RussianTheaterSA6Spawn = { Spawner("SA6"), "SA6" }
RussianTheaterEWRSpawn = { Spawner("EWR"), "EWR" }
RussianTheaterC2Spawn = { Spawner("C2"), "C2" }
RussianTheaterAWACSSpawn = Spawner("A50")
RussianTheaterAWACSPatrol = Spawner("MiG29-RUSAWACS Patrol")
RussianTheaterTankerSpawn = Spawner("RED TANKER")
RussianTheaterTankerPatrol = Spawner("MiG29-RUSTANKER Patrol")

-- CAP Redfor spawns
RussianTheaterMig212ShipSpawn = Spawner("Mig21-2ship")
RussianTheaterMig252ShipSpawn = Spawner("Mig25 2ship")
RussianTheaterF42ShipSpawn = Spawner("F-4 2ship")
RussianTheaterMig292ShipSpawn = Spawner("Mig29-2ship")
RussianTheaterSu272sShipSpawn = Spawner("Su27-2ship")
RussianTheaterF142ShipSpawn = Spawner("F14-2ship")
RussianTheaterF5Spawn = Spawner("f52ship")

Mig23MLD2ShipSpawn = Spawner("Mig-23MLD-2ship")
Su302ShipSpawn = Spawner("Su30-2ship")

RussianTheaterMig212ShipSpawnGROUND = Spawner("Mig21-2shipGROUND")
RussianTheaterMig292ShipSpawnGROUND = Spawner("Mig29-2shipGROUND")
RussianTheaterSu272sShipSpawnGROUND = Spawner("Su27-2shipGROUND")
RussianTheaterF5SpawnGROUND = Spawner("f52shipGROUND")
RussianTheaterF142ShipSpawnGROUND = Spawner("F14-2shipGROUND")

RussianTheaterMig312ShipSpawn = Spawner("Mig31-2ship")

RussianTheaterMig312ShipSpawn:OnSpawnGroup(function(spawned_group)
    table.insert(enemy_interceptors, spawned_group:getName())
end)

--BAI Target Spawns
AssaultSpawn = { Spawner("assault_1_1"), "ASSAULT"}
RussianHeavyArtySpawn = { Spawner("ARTILLERY"), "ARTILLERY" }
baispawns = { AssaultSpawn, RussianHeavyArtySpawn }
SAMSpawns = {
    { RussianTheaterSA10Spawn, getSA10Zone },
    { RussianTheaterSA6Spawn, getSA6Zone }
}

PrimeBAISpawns = function(spawner)
    log("Priming spawner for " .. spawner[2])
    spawner[1]:OnSpawnGroup(function(grp)
        local callsign = getCallsign()
        log("Spawning BAI: " .. spawner[2] .. " as " .. callsign)
        AddObjective("BAI", getMarkerId())(grp, spawner[2], callsign)
        GameStats:increment("bai")
    end)
end
for i,v in ipairs(baispawns) do
    PrimeBAISpawns(v)
end

-- Strike Target Spawns
AmmoDumpDef = Spawner("Ammo DumpDEF")
CommsArrayDef = Spawner("Comms ArrayDEF")
PowerPlantDef = Spawner("Power PlantDEF")

AmmoDumpSpawn = StaticSpawner("Ammo Dump", 7, {
    {0, 0},
    {40, 0},
    {80, -50},
    {80, 0},
    {90, 50},
    {0, 90},
    {-90, 0}
})

AmmoDumpSpawn:OnSpawnGroup(function(staticNames, pos)
    local callsign = getCallsign()
    AddStrikeObjective(getMarkerId(), callsign, "AmmoDump", staticNames)

    --Offset to get the defense group in the right location (318° for 83m)
    --defense_offset = {-55.5, 61.7}
    SpawnStaticDefense("Ammo DumpDEF", pos, {-55.5, 61.7})
    GameStats:increment("ammo")
end)

CommsArraySpawn = StaticSpawner("Comms Array", 3, {
    {0, 0},
    {80, 0},
    {80, -50},
})

CommsArraySpawn:OnSpawnGroup(function(staticNames, pos)
    local callsign = getCallsign()
    AddStrikeObjective(getMarkerId(), callsign, "CommsArray", staticNames)

    --Offset to get the defense group in the right location (319° for 73m)
    --defense_offset = {-47.9, 55.1}
    SpawnStaticDefense("Comms ArrayDEF", pos, {-47.9, 55.1})
    GameStats:increment("comms")
end)

PowerPlantSpawn = StaticSpawner("Power Plant", 7, {
    {0, 0},
    {100, 0},
    {200, 150},
    {400, 150},
    {130,  200},
    {160, 200},
    {190, 200}
})

PowerPlantSpawn:OnSpawnGroup(function(staticNames, pos)
    local callsign = getCallsign()
    AddStrikeObjective(getMarkerId(), callsign, "PowerPlant", staticNames)

    --Offset to get the defense group in the right location (43° for 370m)
    --defense_offset = {270.6, 272.3}
    SpawnStaticDefense("Power PlantDEF", pos, {270.6, 292.3})
end)

SpawnStaticDefense = function(group_name, position, defense_offset)
    --Inputs
    --  group_name : str
    --    The group to spawn in the Mission Editor.
    --  position : table {x, y}
    --    The position of the strike target.
    --  defense_offset : table {x, y} (Optional)
    --    The offset of the defense force with respect to the strike target position.
    --    Ommiting this will potentially cause the first defense unit to spawn on
    --    top of strike target buildings.

    if not defense_offset then defense_offset = {0., 0.} end

    local groupData = mist.getGroupData(group_name)
    local leaderPos = {groupData.units[1].x, groupData.units[1].y}
    for i,unit in ipairs(groupData.units) do
        local separation = {}
        separation[1] = unit.x - leaderPos[1]
        separation[2] = unit.y - leaderPos[2]
        unit.x = position[1] + separation[1] + defense_offset[1]
        unit.y = position[2] + separation[2] + defense_offset[2]
    end

    groupData.clone = true
    mist.dynAdd(groupData)
end

-- Naval Strike target Spawns
--PlatformGroupSpawn = {SPAWNSTATIC:NewFromStatic("Oil Platform", country.id.RUSSIA), "Oil Platform"}

-- Airfield CAS Spawns
RussianTheaterCASSpawn = Spawner("Su25T-CASGroup")
RussianTheaterCASSU24Spawn = Spawner("Su24-CASGroup")
RussianTheaterCASSpawnF14 = Spawner("F14-CASGroup")

-- FARP Support Groups
FSW = Spawner("FARP Support West")

-- Group spanws for easy randomization
local allcaps = {
    RussianTheaterMig212ShipSpawn, RussianTheaterSu272sShipSpawn, RussianTheaterMig292ShipSpawn, RussianTheaterF5Spawn, RussianTheaterMig252ShipSpawn,
    RussianTheaterMig212ShipSpawnGROUND, RussianTheaterSu272sShipSpawnGROUND, RussianTheaterMig292ShipSpawnGROUND, RussianTheaterF5SpawnGROUND,
    RussianTheaterF142ShipSpawn, RussianTheaterF142ShipSpawnGROUND, Mig23MLD2ShipSpawn, Su302ShipSpawn
}
poopcaps = {RussianTheaterMig212ShipSpawn, RussianTheaterF5Spawn, RussianTheaterF42ShipSpawn, Mig23MLD2ShipSpawn, RussianTheaterMig252ShipSpawn}
goodcaps = {RussianTheaterMig292ShipSpawn, RussianTheaterSu272sShipSpawn, RussianTheaterF142ShipSpawn, Su302ShipSpawn}
poopcapsground = {RussianTheaterMig212ShipSpawnGROUND, RussianTheaterF5SpawnGROUND}
goodcapsground = {RussianTheaterMig292ShipSpawnGROUND, RussianTheaterSu272sShipSpawnGROUND, RussianTheaterF142ShipSpawnGROUND}

local hasRadioBeacon = { }

function activateLogi(logiAreaName)
    log("Activating logistics zone for " .. logiAreaName)
    local zoneName = "Logi " .. logiAreaName
    local zone = trigger.misc.getZone(zoneName)
    if not zone then return end
    local statictable = mist.utils.deepCopy(logispawn)
    statictable.x = zone.point.x
    statictable.y = zone.point.z
    local static = mist.dynAddStatic(statictable)
    table.insert(ctld.logisticUnits, static.name)
    ctld.activatePickupZone(zoneName)

    if not hasRadioBeacon[zoneName] == true then
        log("Spawning radio beacon for " .. zoneName)
        ctld.createRadioBeaconAtZone(zoneName, "blue", 1440, zoneName)
        hasRadioBeacon[zoneName] = true
    end
end

function deactivateLogiZone(logiAreaName)
    log("Deactivating logistics zone for " .. logiAreaName)
    -- Don't destroy the unit.. it's probably dead already.
    -- We do want to make sure it's actually a zone before handing
    -- it off to CTLD.
    local zone = trigger.misc.getZone(logiAreaName)
    if not zone then return end
    ctld.deactivatePickupZone(logiAreaName)
end

RussianTheaterAWACSSpawn:OnSpawnGroup(function(SpawnedGroup)
    local callsign = "Overseer"
    AddObjective("AWACS", getMarkerId())(SpawnedGroup, "AWACS", callsign)
    RussianTheaterAWACSPatrol:Spawn()
    GameStats:increment("awacs")
end)

RussianTheaterSA6Spawn[1]:OnSpawnGroup(function(SpawnedGroup)
    local callsign = getCallsign()
    AddObjective("StrategicSAM", getMarkerId())(SpawnedGroup, RussianTheaterSA6Spawn[2], callsign)
    buildCheckSAMEvent(SpawnedGroup, callsign)
    GameStats:increment("sam")
end)

RussianTheaterSA10Spawn[1]:OnSpawnGroup(function(SpawnedGroup)
    local callsign = getCallsign()
    AddObjective("StrategicSAM", getMarkerId())(SpawnedGroup, RussianTheaterSA10Spawn[2], callsign)
    buildCheckSAMEvent(SpawnedGroup, callsign)
    GameStats:increment("sam")
end)

RussianTheaterEWRSpawn[1]:OnSpawnGroup(function(SpawnedGroup)
    local callsign = getCallsign()
    AddObjective("EWR", getMarkerId())(SpawnedGroup, RussianTheaterEWRSpawn[2], callsign)
    buildCheckEWREvent(SpawnedGroup, callsign)
    GameStats:increment("ewr")
end)

RussianTheaterC2Spawn[1]:OnSpawnGroup(function(SpawnedGroup)
    local callsign = getCallsign()
    AddObjective("C2", getMarkerId())(SpawnedGroup, RussianTheaterC2Spawn[2], callsign)
    buildCheckC2Event(SpawnedGroup, callsign)
    GameStats:increment("c2")
end)

SpawnOPFORCas = function(spawn)
    --log("===== CAS Spawn begin")
    local casGroup = spawn:Spawn()
end


for i,v in ipairs(allcaps) do
    v:OnSpawnGroup(function(SpawnedGroup)
        AddRussianTheaterCAP(SpawnedGroup)
        GameStats:increment("caps")
    end)
end

activeBlueXports = {}

addToActiveBlueXports = function(group, defense_group_spawner, target, is_farp, xport_data, logiunit)
    activeBlueXports[group:getName()] = {defense_group_spawner, target, is_farp, xport_data, logiunit}
    log("Added " .. group:getName() .. " to active blue transports")
end

removeFromActiveBlueXports = function(group, defense_group_spawner, target)
    activeBlueXports[group:getName()] = nil
end

for name,spawn in pairs(NorthGeorgiaTransportSpawns) do
    for i=1,2 do
        if i == 1 then
            spawn[i]:OnSpawnGroup(function(SpawnedGroup)
                addToActiveBlueXports(SpawnedGroup, AirfieldDefense, name, false, spawn[i], spawn[3])
            end)
        end

        if i == 2 then
            spawn[i]:OnSpawnGroup(function(SpawnedGroup)
                addToActiveBlueXports(SpawnedGroup, AirfieldDefense, name, false, spawn[i], spawn[3])
            end)
        end

    end
end

for name,spawn in pairs(NorthGeorgiaFARPTransportSpawns) do
    spawn[1]:OnSpawnGroup(function(SpawnedGroup)
        addToActiveBlueXports(SpawnedGroup, AirfieldDefense, name, true, spawn[1], spawn[3])
    end)
end


base_captured_event_analyzer = function(e)
    if e.id == world.event.S_EVENT_BASE_CAPTURED then
        local place = e.place
        if e.initiator == nil then
            log("BASECAPTURED -- nil initiator. Place is [".. place:getName() .. "]")
        else
            local init = e.initiator
            log("BASECAPTURED -- Active initiator. Place is [" .. place:getName() .. "] and initiator [" .. mist.utils.tableShow(init) .. "]")
        end
    end
end
mist.addEventHandler(base_captured_event_analyzer)
log("spawns.lua complete")
