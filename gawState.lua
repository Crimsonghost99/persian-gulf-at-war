-- Setup an initial state object and provide functions for manipulating that state.
objective_spawn_zones = {
    ["spawn_1_1"] = Airbase.getByName("Al Maktoum Intl"),
    ["spawn_1_2"] = Airbase.getByName("Al Maktoum Intl"),
    ["spawn_1_3"] = Airbase.getByName("Al Maktoum Intl"),
    ["spawn_2_1"] = Airbase.getByName("Dubai Intl"),
    ["spawn_2_2"] = Airbase.getByName("Fujairah Intl"),
    ["spawn_2_3"] = Airbase.getByName("Sharjah Intl"),
    ["spawn_3_1"] = Airbase.getByName("Khasab"),
    ["spawn_3_2"] = Airbase.getByName("Khasab"),
    ["spawn_3_3"] = Airbase.getByName("Khasab"),
}

game_state = {
    ["last_launched_time"] = 0,
    ["last_cap_spawn"] = 0,
    ["Phase Airfields"] = {},
    ["Airfields"] = {},
    ["Airfield Defenders"] = {},
    ["Primary"] = {
        ["Bandar Abbas Intl"] = false,
    },
    ["StrategicSAM"] = {},
    ["C2"] = {},
    ["EWR"] = {},
    ["CASTargets"] = {},
    ["StrikeTargets"] = {},
    ["NavalStrike"] = {},
    ["InterceptTargets"] = {},
    ["DestroyedStatics"] = {},
    ["OpforCAS"] = {},
    ["CAP"] = {},
    ["BAI"] = {},
    ["AWACS"] = {},
    ["Tanker"] = {},
    ["NavalStrike"] = {},
    ["CTLD_ASSETS"] = {},
    ['Convoys'] ={},
    ["last_redfor_cap"] = 0,
    ["FARPS"] = {},
    ["victory"] = false
}


local ignored_airfields = {
    --We don't want these to appear in the 'Airfields' list in the state.
    "Stennis",
    "Tarawa",
    "Oliver Hazzard"
}
local function is_ignored_airfield(abName)
    for _,s in ipairs(ignored_airfields) do
        if string.match(abName, s) then return true end
    end
    return false
end
local airfields
for i=0,2 do
    airfields = coalition.getAirbases(i)
    for idx,ab in ipairs(airfields) do
        local name = ab:getName()
        if string.match(name, 'FARP') then
            game_state["FARPS"][name] = i
        elseif not is_ignored_airfield(name) then
            game_state["Airfields"][name] = i
        end
    end
end

Airfield_Spawn_Points = {
    ["Sas Al Nakheel"] = { x = -189901, y = -176838 },
    ["Sir Abu Nuayr"] = { x = -101733, y = -203398 },
    ["Abu Musa Island"] = { x = -32288, y = -121230 },
    ["Tunb Kochak"] = { x = 8591, y = -109288 },
    ["Tunb Island AFB"] = { x = 11371, y = -93219 },
    ["Sirri Island"] = { x = -26896, y = 172288 },
    ["Lar Airbase"] = { x = 168020, y = 181417 },
    ["Bandar Lengeh"] = { x = 42400, y = 141016 },
    ["Sharjah Intl"] = { x = -91999, y = -72970 },
    ["Kerman Airport"] = { x = 454047, y = 70385 },
    ["Fujairah Intl"] = { x = -117986, y = 7336 },
    ["Dubai Intl"] = { x = -99050, y = -89619 },
    ["Al Maktoum Intl"] = { x = -139015, y = -109218 },
    ["Al-Bateen"] = { x = -191932, y = -182483 },
    ["Abu Dhabi Intl"] = { x = -188001, y = -161414 },
    ["Al Ain Intl"] = { x = -210324, y = -66347 },
    ["Qeshm Island"] = { x = 64366, y = -32317 },
    ["Ras Al Khaimah Intl"] = { x = -62209, y = -30032 },
    ["Khasab"] = { x = -709, y = 346 },
    ["Al Minhad AFB"] = { x = -125326, y = -89130 },
    ["FARP ALPHA"] = { x = -163028, y = -119576 },
    ["FARP BRAVO"] = {x = -158511, y = -63721 },
    ["FARP Charlie"] = {x = -44993, y = -32313 },
    ["FARP Delta"] = {x = -61245, y = 2907 }
}

game_stats = {
    c2    = {
        alive = 0,
        nominal = 6,
        tbl   = game_state["C2"],
    },
    sam = {
        alive = 0,
        nominal = 7,
        tbl   = game_state["StrategicSAM"]
    },
    ewr = {
        alive = 0,
        nominal = 6,
        tbl   = game_state["EWR"],
    },
    awacs = {
        alive = 0,
        nominal = 1,
        tbl   = game_state["AWACS"],
    },
    bai = {
        alive = 0,
        nominal = 5,
        constructing_sam = false,
        tbl = game_state["BAI"],
    },
    ammo = {
        alive = 0,
        nominal = 4,
        tbl   = game_state["StrikeTargets"],
        subtype = "AmmoDump",
    },
    comms = {
        alive = 0,
        nominal = 5,
        tbl   = game_state["StrikeTargets"],
        subtype = "CommsArray",
    },
    caps = {
        alive = 0,
        nominal = 6,
        tbl = game_state["CAP"],
    },
    airports = {
        alive = 0,
        nominal = 3,
        tbl = game_state["Airfields"],
    },
    scuds = {
        active = false,
        fired = false
    }
}

game_state["Phase Airfields"] =  {
    {Airbase.getByName("Al Maktoum Intl"), Airbase.getByName("FARP ALPHA"), Airbase.getByName("FARP BRAVO")},
    {Airbase.getByName("Al Minhad AFB"), Airbase.getByName("Dubai Intl"), Airbase.getByName("Fujairah Intl"), Airbase.getByName("Sharjah Intl"), Airbase.getByName("Abu Musa Island")},
    {Airbase.getByName("FARP Charlie"), Airbase.getByName("FARP Delta"), Airbase.getByName("Khasab"), Airbase.getByName("FARP Qeshm"), Airbase.getByName("Qeshm Island"), Airbase.getByName("Tunb Island AFB")},
    {Airbase.getByName("Bandar Abbas Intl"), Airbase.getByName("Havadarya"), Airbase.getByName("Lar"), Airbase.getByName("Kish Intl")}
}

-- Checks whether the mission is considered a victory.
-- NOTE: The current victory status is determined by airfield and
-- farp ownership. This system is not completely reliable as
-- base owners flip during a player connecting. I urge that more
-- than one attempt be checked against this after some time has
-- passed, so that bases ownership flapping will be less likely
-- to cause any errors.
isVictory = function()
    for i,phase in pairs(game_state["Phase Airfields"]) do
        log("VICTORY -- Checking phase " .. i)
        for _,airfield in pairs(phase) do
            if airfield:getCoalition() ~= 2 then
                log("VICTORY -- Not captured airfield " .. airfield:getName() .. " [" .. airfield:getCoalition() .. "]")
                return false
            end
        end
    end
    return true
end


late_unlockables = {
    "Al Minhad AFB A-10C - 1",
    "Al Minhad AFB A-10C - 2",
    "Al Minhad AFB A-10C - 3",
    "Al Minhad AFB A-10C - 4",
    "Al Minhad AFB A-10C_2 - 1",
    "Al Minhad AFB A-10C_2 - 2",
    "Al Minhad AFB A-10C_2 - 3",
    "Al Minhad AFB AV-8B - 1",
    "Al Minhad AFB AV-8B - 2",
    "Al Minhad AFB AV-8B - 3",
    "Al Minhad AFB AV-8B - 4",
    "Al Minhad AFB Gazelle L - 1",
    "Al Minhad AFB Gazelle M - 1",
    "Al Minhad AFB Gazelle M - 2",
    "Al Minhad AFB Gazelle Mistral",
    "Al Minhad AFB Ka-50 - 1",
    "Al Minhad AFB Ka-50 - 2",
    "Al Minhad AFB Ka-50 III - 1",
    "Al Minhad AFB Ka-50 III - 2",
    "Al Minhad AFB Hind 1-1",
    "Al Minhad AFB Hind 1-2",
    "Al Minhad AFB Mi-8 - 1",
    "Al Minhad AFB Mi-8 - 2",
    "Al Minhad AFB Mi-8 - 3",
    "Al Minhad AFB Mi-8 - 4",
    "Al Minhad AFB UH-1H - 1",
    "Al Minhad AFB UH-1H - 2",
    "Al Minhad AFB UH-1H - 3",
    "Al Minhad AFB UH-1H - 4",
    "Al Minhad AFB Yak-52 - 1",
    "Al Minhad AFB Yak-52 - 2",
    "Al Minhad AFB TF-51D - 1",
    "Al Minhad AFB TF-51D - 2",
    "Al Minhad AFB C-101EB - 1",
    "Al Minhad AFB C-101EB - 2",
    "Al Minhad AFB Apache 1",
    "Al Minhad AFB Apache 2",
    "Al Minhad AFB Apache 3",
    "Al Minhad AFB Apache 4",
    "FARP Charlie Gazelle L - 1",
    "FARP Charlie Gazelle M - 1",
    "FARP Charlie Gazelle M - 2",
    "FARP Charlie Gazelle Mistral",
    "FARP Charlie Ka-50 - 1",
    "FARP Charlie Ka-50 - 2",
    "FARP Charlie Ka-50 III - 1",
    "FARP Charlie Ka-50 III - 2",
    "FARP Charlie Hind 1-1",
    "FARP Charlie Hind 1-2",
    "FARP Charlie Mi-8 - 1",
    "FARP Charlie Mi-8 - 2",
    "FARP Charlie UH-1H - 1",
    "FARP Charlie UH-1H - 2",
    "FARP Charlie Apache - 1",
    "FARP Charlie Apache - 2",
    "FARP Charlie Apache - 3",
    "FARP Charlie Apache - 4",
    "FARP Delta Gazelle L - 1",
    "FARP Delta Gazelle M - 1",
    "FARP Delta Gazelle M - 2",
    "FARP Delta Gazelle Mistral",
    "FARP Delta Ka-50 - 1",
    "FARP Delta Ka-50 - 2",
    "FARP Delta Ka-50 III - 1",
    "FARP Delta Ka-50 III - 2",
    "FARP Delta Mi-8 - 1",
    "FARP Delta Mi-8 - 2",
    "FARP Delta UH-1H - 1",
    "FARP Delta UH-1H - 2",
    "FARP Delta Mi-24P",
    "FARP Delta Mi-24P 2",
    "FARP Delta Apache - 1",
    "FARP Delta Apache - 2",
    "FARP Delta Apache - 3",
    "FARP Delta Apache - 4",
    "FARP Qeshm Mi-8 - 1",
    "FARP Qeshm Mi-8 - 2",
    "FARP Qeshm Mi-8 - 3",
    "FARP Qeshm Mi-8 - 4",
    "FARP Qeshm UH-1H - 1",
    "FARP Qeshm UH-1H - 2",
    "FARP Qeshm UH-1H - 3",
    "FARP Qeshm UH-1H - 4",
    "FARP Qeshm Apache - 1",
    "FARP Qeshm Apache - 2",
    "FARP Qeshm Apache - 3",
    "FARP Qeshm Apache - 4",
    "Fujairah Intl Gazelle L - 1",
    "Fujairah Intl Gazelle M - 1",
    "Fujairah Intl Gazelle Mistral",
    "Fujairah Intl Ka-50",
    "Fujairah Intl Ka-50 III",
    "Fujairah Intl Mi-8 - 1",
    "Fujairah Intl Mi-8 - 2",
    "Fujairah Intl UH-1H - 1",
    "Fujairah Intl UH-1H - 2",
    "Khasab A-10C - 1",
    "Khasab A-10C - 2",
    "Khasab A-10C - 3",
    "Khasab A-10C_2 - 1",
    "Khasab A-10C_2 - 2",
    "Khasab A-10C_2 - 3",
    "Khasab AV-8B - 1",
    "Khasab AV-8B - 2",
    "Khasab AV-8B - 3",
    "Khasab AV-8B - 4",
    "Khasab Ka-50 - 1",
    "Khasab Ka-50 - 3",
    "Khasab Ka-50 - 4",
    "Khasab Ka-50 III",
    "Khasab Hind 1-1",
    "Khasab Hind 1-2",
    "Khasab Mi-8 - 1",
    "Khasab Mi-8 - 2",
    "Khasab Mi-8 - 3",
    "Khasab Mi-8 - 4",
    "Khasab UH-1H - 1",
    "Khasab UH-1H - 2",
    "Khasab UH-1H - 3",
    "Khasab UH-1H - 4",
    "Khasab Yak-52 - 1",
    "Khasab Yak-52 - 2",
    "Khasab TF-51D - 1",
    "Khasab TF-51D - 2",
    "Khasab C-101EB - 1",
    "Khasab C-101EB - 2",
    "Khasab Gazelle L - 1",
    "Khasab Gazelle M - 1",
    "Khasab Gazelle M - 2",
    "Khasab Gazelle Mistral",
    "Khasab Su-25T - 1",
    "Khasab Su-25T - 2",
    "Khasab Apache - 1",
    "Khasab Apache - 2",
    "Khasab Apache - 3",
    "Khasab Apache - 4",
    "Qeshm Island Gazelle L - 1",
    "Qeshm Island Gazelle L - 2",
    "Qeshm Island Gazelle M - 1",
    "Qeshm Island Gazelle M - 2",
    "Qeshm Island Gazelle Mistral",
    "Qeshm Island Ka-50 - 1",
    "Qeshm Island Ka-50 - 2",
    "Qeshm Island Ka-50 III",
    "Qeshm Island Ka-50 III - 2",
    "Qeshm Island Hind 1-1",
    "Qeshm Island Hind 1-2",
    "Qeshm Island Apache-1",
    "Qeshm Island Apache-2",
    "Qeshm Island Apache-3",
    "Qeshm Island Apache-4",
}

startVictoryDisplay = function()
    mist.scheduleFunction(function()
        local output = "===========================================================\n"..
        "Congratulations! All Primary Objectives have been completed!\n"..
        "The server will automatically restart with a fresh state momentarily.\n\n"..
        "Good work!"

        MessageToAll(output, 30, true)
    end,{},timer.getTime() + 1, 30)
end

log("Game State INIT")
