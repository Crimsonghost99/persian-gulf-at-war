## Local Development
1. From your install folder (not saved games), open scripts/MissionScripting.lua
2. Comment out all the lines in the do block below the sanitization function with "-\-".  This allows the lua engine access to the file system. It should look similar to:

        --sanitizeModule('os')
	    --sanitizeModule('io')
	    --sanitizeModule('lfs')
	    --require = nil
	    --loadlib = nil
3. Clone this repo.  From your `Saved Games\DCS.openbeta\Scripts` folder run `git clone https://gitlab.com/hoggit/developers/persian-gulf-at-war.git PGAW`.  This should create a folder named `PGAW` and in the end it should look like `Saved Games\DCS.openbeta\Scripts\PGAW
`
4. Download the .miz file from [Google Drive](https://drive.google.com/file/d/1DioYmea3GTWoOdPkF5DE9Dx7A7mcuBbX/view?usp=sharing)
