CLEANUP_PERIOD = 125
CLEANUP_CAP_PERIOD = 600
last_cap_cleanup_time = env.mission.start_time

function cleanup()
    local current_time = timer.getAbsTime() + env.mission.start_time

    log("Starting Cleanup AP DEF Groups")
    -- Go through groups in coalition 1 (red), of type 2 (GROUND)
    local surviving_ap = 0
    for template_name, group_name in pairs(game_state["Airfield Defenders"]) do
        local group = Group.getByName(group_name)
        if not group or not isAlive(group) or (unitsAliveInGroup(group) / group:getInitialSize() * 100 < 30) then
            log("AP DEF group " .. group_name .. " (Spawned from template:".. template_name ..") is no longer among us.  Removing from state")
            game_state["Airfield Defenders"][template_name] = nil
        else
            surviving_ap = surviving_ap + 1
            log("AP DEF group " .. group_name .. " (Spawned from template:".. template_name ..") is doing just fine.  Doing nothing.")
        end
    end
    log("There are " .. surviving_ap .. " AP DEF groups remaining")
    env.info("CLEANUP -- ["..surviving_ap.."] AP DEF groups remaining.")

    log("Starting Cleanup BAI Targets")
    -- Get Alive BAI Targets and cleanup state
    local baitargets = game_state["BAI"]
    local baitargetcount = 0
    for group_name, baitarget_table in pairs(baitargets) do
        local baitarget = Group.getByName(group_name)
        if baitarget and isAlive(baitarget) then
            local alive_units = unitsAliveInGroup(baitarget)

            log("There are " .. alive_units .. " in BAI target " .. baitarget_table['callsign'])
            if alive_units == 0 or alive_units / baitarget:getInitialSize() * 100 < 30 then
                trigger.action.outText("BAI target " .. baitarget_table['callsign'] .. " destroyed!", 15)
                log("Not enough units, destroying")
                baitarget:destroy()
                baitargets[group_name] = nil
                removeObjectiveMark(baitarget_table)
                GameStats:decrement("bai")
            end
            baitargetcount = baitargetcount + 1
        else
            --for i,rearm_spawn in ipairs(rearm_spawns) do
            --    rearm_spawn[1]:Spawn()
            -- end
            trigger.action.outText("BAI target " .. baitarget_table['callsign'] .. " destroyed!", 15)
            baitargets[group_name] = nil
            GameStats:decrement("bai")
        end
    end
    env.info("CLEANUP -- ["..baitargetcount.."] BAI targets remaining.")

    local radarcount = 0
    local launchercount = 0
    local samcount = 0
    for group_name,sam_data in pairs(game_state["StrategicSAM"]) do
        -- Get the group to make sure its alive still.
        local grp = Group.getByName(group_name)
        if grp then
            samcount = samcount + 1
            for _, unit in pairs(grp:getUnits()) do
                local type_name = unit:getTypeName()
                if type_name == "Kub 2P25 ln" then launchercount = launchercount + 1 end
                if type_name == "Kub 1S91 str" then radarcount = radarcount + 1 end
                if type_name == "S-300PS 64H6E sr" then radarcount = radarcount + 1 end
                if type_name == "S-300PS 40B6MD sr" then radarcount = radarcount + 1 end
                if type_name == "S-300PS 40B6M tr" then radarcount = radarcount + 1 end
                if type_name == "S-300PS 5P85C ln" then launchercount = launchercount + 1 end
                if type_name == "S-300PS 5P85D ln" then launchercount = launchercount + 1 end
            end
        end
    end
    env.info("CLEANUP -- [".. samcount .."] SAM sites remaining.")
    env.info("CLEANUP -- [".. radarcount .."] SAM radars remaining.")
    env.info("CLEANUP -- [".. launchercount .."] SAM launchers remaining.")

    log("Starting Cleanup C2")
    -- Get the number of C2s in existance, and cleanup the state for dead ones.
    local c2s = game_state["C2"]
    local c2scount = 0
    for group_name, group_table in pairs(c2s) do
        local callsign = group_table['callsign']
        if groupIsDead(group_name) then
            trigger.action.outText("Mobile CP " .. callsign .. " destroyed!", 15)
            removeObjectiveMark(group_table)
            game_state["C2"][group_name] = nil
            GameStats:decrement("c2")
        end
        c2scount = c2scount + 1
    end
    env.info("CLEANUP -- ["..c2scount.."] C2 targets remaining.")

    log("Starting Strike Cleanup")
    -- Get the number of Strikes in existance, and cleanup the state for dead ones.
    local striketargets = game_state["StrikeTargets"]
    local striketargetcount = 0
    for group_name, group_table in pairs(striketargets) do
        local alive_units = 0
        for _,staticname in ipairs(group_table.statics) do
            local staticunit = StaticObject.getByName(staticname)
            if staticunit and staticunit:getLife() > 0 and staticunit:isExist() then
                alive_units = alive_units + 1
            end
        end

        if alive_units == 0 then
            trigger.action.outText("Strike Target " .. group_table['callsign'] .. " destroyed!", 15)
            removeObjectiveMark(group_table)
            game_state["StrikeTargets"][group_name] = nil
        else
            log(group_name .. " has " .. alive_units .. " buildings alive.")
            striketargetcount = striketargetcount + 1
        end
    end
    log("Done Strike Cleanup. ["..striketargetcount.."] strike targets remaining.")
    env.info("CLEANUP -- ["..striketargetcount.."] strike targets remaining.")
    log("SHIPS -- Checking Ship Status")
    local shipState = game_state["Ships"]
    for _,shipName in pairs(ShipGroupsTrackedByState) do
        local shipData = shipState[shipName]
        log("SHIPS -- Checking ship ["..shipName.."] status.")
        if shipData and shipData["alive"] then
            local grpname = shipData["groupName"]
            if shipIsDead(grpname) then
                log("SHIPS -- Ship ["..shipName.."] is dead. Marking as dead in the state.")
                shipState[shipName] = {
                    alive = false,
                    groupName = grpname
                }
            else
                log("SHIPS -- Ship ["..shipName.."] is still alive. Not doing anything.")
            end
        else
            log("SHIPS -- ["..shipName.."] either already dead or missing. Not doing anything.")
        end
    end
    game_state["Ships"] = shipState

    local staticsAllDead = function(statics)
        for _,static in pairs(statics) do
            local dcs_static = StaticObject.getByName(static.name)
            if dcs_static and dcs_static:isExist() and dcs_static:getLife() > 0 then
                return false
            end
        end
        return true
    end

    log("Checking Naval Strikes")
    local new_naval_strikes = {}
    for name, data in pairs(game_state["NavalStrike"]) do
        log("\tChecking " .. name)
        if staticsAllDead(data["statics"]) then
            log("\t"..name.." are all dead.")
            removeObjectiveMark(data)
            MessageToAll("Objective ".. data["callsign"] .. " has been destroyed!")
        else
            log("\t"..name.." has at least one static alive.")
            new_naval_strikes[name] = data
        end
    end
    game_state["NavalStrike"] = new_naval_strikes

    if (current_time - last_cap_cleanup_time) >= CLEANUP_CAP_PERIOD then
        last_cap_cleanup_time = current_time

        local caps = game_state["CAP"]
        for i=#caps, 1, -1 do
            local cap = Group.getByName(caps[i])
            if cap and isAlive(cap) then
                if allOnGround(cap) then
                    cap:destroy()
                    log("Found inactive cap, removing")
                    table.remove(caps, i)
                    GameStats:decrement("caps")
                end
            else
                table.remove(caps, i)
                GameStats:decrement("caps")
            end
        end

        for i,g in ipairs(enemy_interceptors) do
            if allOnGround(g) then
                Group.getByName(g):destroy()
            end

            if not isAlive(g) then
                enemy_interceptors = {}
            end
        end
    end
    log("Cleanup script finished")
    -- Check number of red planes
    local mut = mist.makeUnitTable({"[red][plane]"})
    local unitCounts = {}
    for _,unitName in pairs(mut) do
        local unit = Unit.getByName(unitName)
        log("CLEANUP -- Counting unit ["..unitName.."]")
        if unit and unit:isActive() then
            log("CLEANUP -- Unit ["..unitName.."] is active.")
            local type_name = unit:getTypeName()
            if unitCounts[type_name] == nil then unitCounts[type_name] = 0 end
            unitCounts[type_name] = unitCounts[type_name] + 1
        else
            log("CLEANUP -- Unit ["..unitName.."] is not active disregarding.")
        end
    end
    for typeName,count in pairs(unitCounts) do
        env.info("CLEANUP -- [".. count .."] " .. typeName)
    end


    --Check for a victory
    if isVictory() then
        log("VICTORY -- Victory is true. Checking again in 10 seconds...")
        mist.scheduleFunction(function()
            log("VICTORY -- Double checking... ")
            if isVictory() then
                game_state["victory"] = true
                startVictoryDisplay()
                log("VICTORY -- CONFIRMED")
                write_state()
            end
        end, {}, timer.getTime() + 10)
    end
    write_state()
end

mist.scheduleFunction(cleanup, {}, timer.getTime() + 47, CLEANUP_PERIOD)
