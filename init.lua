local module_folder = lfs.writedir()..[[Scripts\PGAW\]]
package.path = module_folder .. "?.lua;" .. package.path
local ctld_config = require("ctld_config")

local statefile = io.open(lfs.writedir() .. "Scripts\\PGAW\\state.json", 'r')

-- Enable slotblock
trigger.action.setUserFlag("SSB",100)


-- remove nukes
require("slmod_kick_for_nukes")
require("Weapons_Damage_Updated")


--Enable Weapon Points

local WP = require("weapon_points.weapon_points")

local wpConfigFile = io.open(lfs.writedir() .. "Scripts\\PGAW\\weapon_points.json", 'r')
if wpConfigFile then
    log("WEAPONPOINTS - Loading config")
    local wpConfig = wpConfigFile:read("*all")
    wpConfigFile:close()
    WP.LoadJsonConfig(wpConfig, true)
    log("WEAPONPOINT - Loaded Config")
end

-- Disable slots
for _,ac in ipairs(late_unlockables) do
    trigger.action.setUserFlag(ac,100)
    log("Disabled slot ["..ac.."].")
end
if statefile then
    trigger.action.outText("Found a statefile.  Processing it instead of starting a new game", 5)
    local state = statefile:read("*all")
    statefile:close()
    local saved_game_state = json:decode(state)
    -- trigger.action.outText("Game state read", 10)

    -- Spawn any defenders not killed last round
    for template_name, group_name in pairs(saved_game_state["Airfield Defenders"]) do
        local new_grp = Spawner(template_name):Spawn()
        if not new_grp or not isAlive(new_grp) then
            log("Failed to spawn AP DEF group " .. template_name)
        end
        game_state["Airfield Defenders"][template_name] = new_grp:getName()
        log("Spawned surviving AP DEF group " .. template_name .. " as " .. new_grp:getName())
    end

    -- Spawn a defense force if blue owns an airfield.  Set the slotblock flag for the airfield if it needs it
    for name, coalition in pairs(saved_game_state["Airfields"]) do
        if name ~= ''
            and not string.match(name, "Bandar-e-Jask")
            and not string.match(name, "Lavan")
            and not string.match(name, "Liwa")
            and not string.match(name, "Jiroft")
            and not string.match(name, "Unit")
            and not string.match(name, "Al Dhafra AFB")
            and not string.match(name, "George") then
            log("Processing Airfield " .. name)
            local ab = Airbase.getByName(name)
            local apV3 = ab:getPosition().p
            local posx = 0
            local posy = 0
            local lookup = Airfield_Spawn_Points[name]
            if lookup then
                posx = lookup.x
                posy = lookup.y
            else
                posx = apV3.x + math.random(-150, 150)
                posy = apV3.z - math.random(-150, 150)
            end
            game_state["Airfields"][name] = coalition

            if coalition == 2 then
                if not string.match(name, "Al Dhafra AFB") then
                    log("Spawning Airfield Defense at " .. name)
                    -- Spawn a defense force, a support force, and active a logi slot if one exists for this AB
                    AirfieldDefense:SpawnAtPoint({
                        x = posx,
                        y = posy
                    })

                    posx = posx + math.random(-50, 50)
                    posy = posy + math.random(-50, 50)
                    BlueFarpSupportGroups[name] = FSW:SpawnAtPoint({x=posx, y=posy})
                    enableSlotsForAirfield(name)
                    --activateLogi(name)
                end
            end

        end
    end

    -- trigger.action.outText("Finished processing airfields", 10)

    local markerID = 1
    for name, coalition in pairs(saved_game_state["FARPS"]) do
        if not string.match(name, "Abu Dhabi Area FARP") then
            log("Processing FARP " .. name)
            local ab = Airbase.getByName(name)
            local apV3 = ab:getPosition().p

            if string.match("FARP Qeshm", name) then
                trigger.action.markToAll(markerID, name, {x = apV3.x, z = apV3.z+20.0, y = apV3.y}, true)
            else
                trigger.action.markToAll(markerID, name.."\nCapture to provide weapons to Al Dhafra", {x = apV3.x, z = apV3.z+20.0, y = apV3.y}, true)
            end
            markerID = markerID + 1

            apV3.x = apV3.x + math.random(-25, 25)
            apV3.z = apV3.z + math.random(-25, 25)
            game_state["FARPS"][name] = coalition

            if coalition == 2 then
                BlueSecurityForcesGroups[name] = AirfieldDefense:SpawnAtPoint(apV3)
                BlueFarpSupportGroups[name] = FSW:SpawnAtPoint({x=apV3.x, y=apV3.z}, true)
                enableSlotsForAirfield(name)
                --activateLogi(name)
            end

        end
    end

    -- trigger.action.outText("Finished processing FARPs", 10)

    for name, data in pairs(saved_game_state["StrategicSAM"]) do
        local spawn
        if data.spawn_name == "SA6" then spawn = RussianTheaterSA6Spawn[1] end
        if data.spawn_name == "SA10" then spawn = RussianTheaterSA10Spawn[1] end
        spawn:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    for name, data in pairs(saved_game_state["C2"]) do
        RussianTheaterC2Spawn[1]:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    for name, data in pairs(saved_game_state["EWR"]) do
        RussianTheaterEWRSpawn[1]:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    -- trigger.action.outText("Finished processing strategic assets", 10)

    for _, data in pairs(saved_game_state["StrikeTargets"]) do
        local spawn
        log('spawning ' .. data['spawn_name'])
        if data['spawn_name'] == 'AmmoDump' then spawn = AmmoDumpSpawn end
        if data['spawn_name'] == 'CommsArray' then spawn = CommsArraySpawn end
        if data['spawn_name'] == 'PowerPlant' then spawn = PowerPlantSpawn end
        spawn:Spawn({
            data['position'].x,
            data['position'].z
        })
    end

    -- Some backwards compat.
    if saved_game_state["NavalStrike"] == nil then saved_game_state["NavalStrike"] = {} end
    for name,data in pairs(saved_game_state["NavalStrike"]) do
        local pt = mist.utils.makeVec2(data["position"])
        local spawns = SpawnOilRigGroup(pt)
        AddNavalStrikeObjective(data["markerID"], name, data["callsign"], spawns)
    end


    for name, data in pairs(saved_game_state["BAI"]) do
        local spawn
        if data['spawn_name'] == "ASSAULT" then spawn = AssaultSpawn[1] end
        if data['spawn_name'] == "ARTILLERY" then spawn = RussianHeavyArtySpawn[1] end

        log("BAI From State is " .. data['spawn_name'])
        local spawnedGroup = spawn:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
        log("Spawned BAI and scheduling tasking. Spawned Group = [ " .. spawnedGroup:getName() .."]")
    end

    -- trigger.action.outText("Finished processing BAI", 10)


    --LOAD CTLD FROM STATE
    ---------------------------------------------------------------------------

    INIT_CTLD_UNITS = function(args, coords2D, _country, ctld_unitIndex, key)
    --Spawns the CTLD unit at a given point using the ctld_config templates,
    --returning the unit object so that it can be tracked later.
    --
    --Inputs
    --  args : table
    --    The ctld_config unit template to spawn.
    --    Ex. ctld_config.unit_config["M818 Transport"]
    --  coord2D : table {x,y}
    --    The location to spawn the unit at.
    --  _country : int or str
    --    The country ID that the spawned unit will belong to. Ex. 2='USA'
    --  cltd_unitIndex : table
    --    The table of unit indices to help keep track of unit IDs. This table
    --    will be accessed by keys so that the indices are passed by reference
    --    rather than by value.
    --  key : str
    --    The table entry of cltd_unitIndex that will be incremented after a
    --    unit and group name are assigned.
    --    Ex. key = "Gepard_Index"
    --
    --Outputs
    --  Group_Object : obj
    --    A reference to the spawned group object so that it can be tracked.

        local unitNumber = ctld_unitIndex[key]
        local CTLD_Group = {
            ["visible"] = false,
            ["hidden"] = false,
            ["units"] = {
              [1] = {
                ["type"] = args.type,                           --unit type
                ["name"] = args.name .. unitNumber,             --unit name
                ["heading"] = 0,
                ["playerCanDrive"] = args.playerCanDrive,
                ["skill"] = args.skill,
                ["x"] = coords2D.x,
                ["y"] = coords2D.y,
              },
            },
            ["name"] = args.name .. unitNumber,                 --group name
            ["task"] = {},
            ["category"] = Group.Category.GROUND,
            ["country"] = _country                              --group country
        }

        --Debug
        --trigger.action.outTextForCoalition(2,"CTLD Unit: "..CTLD_Group.name, 30)

        --Increment Index and spawn unit
        ctld_unitIndex[key] = unitNumber + 1
        local _spawnedGroup = mist.dynAdd(CTLD_Group)

        return Group.getByName(_spawnedGroup.name)              --Group object
    end


    log("START: Spawning CTLD units from state")
    local ctld_unitIndex = ctld_config.unit_index
    for idx, data in ipairs(saved_game_state["CTLD_ASSETS"]) do

        local coords2D = { x = data.pos.x, y = data.pos.z}
        local country = 2   --USA

        if data.name == 'mlrs' then
            local key = "M270_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["MLRS M270"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'M-109' then
            local key = "M109_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M109 Paladin"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'abrams' then
            local key = "M1A1_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M1A1 Abrams"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'jtac' then
            local key = "JTAC_Index"
            local _spawnedGroup = INIT_CTLD_UNITS(ctld_config.unit_config["HMMWV JTAC"], coords2D, country, ctld_unitIndex, key)

            local _code = table.remove(ctld.jtacGeneratedLaserCodes, 1)
            table.insert(ctld.jtacGeneratedLaserCodes, _code)
            ctld.JTACAutoLase(_spawnedGroup:getName(), _code)
        end

        if data.name == 'ammo' then
            local key = "M818_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M818 Transport"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'gepard' then
            local key = "Gepard_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["Flugabwehrkanonenpanzer Gepard"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'vulcan' then
            local key = "Vulcan_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M163 Vulcan"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'avenger' then
            local key = "Avenger_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M1097 Avenger"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'chaparral' then
            local key = "Chaparral_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["M48 Chaparral"], coords2D, country, ctld_unitIndex, key)
        end

        if data.name == 'roland' then
            local key = "Roland_Index"
            INIT_CTLD_UNITS(ctld_config.unit_config["Roland ADS"], coords2D, country, ctld_unitIndex, key)
        end
    end

    local hawkCTLDstate = saved_game_state["Hawks"]
    if hawkCTLDstate ~= nil then
        for k,v in pairs(hawkCTLDstate) do
            respawnHAWKFromState(v)
        end
    end

    local NASAMSCTLDstate = saved_game_state["NASAMS"]
    if NASAMSCTLDstate ~= nil then
        for k,v in pairs(NASAMSCTLDstate) do
            respawnNASAMSFromState(v)
        end
    end

    game_state["CTLD_ASSETS"] = saved_game_state["CTLD_ASSETS"]
    log("COMPLETE: Spawning CTLD units from state")


    local destroyedStatics = saved_game_state["DestroyedStatics"]
    if destroyedStatics ~= nil then
        for k, v in pairs(destroyedStatics) do
            local obj = StaticObject.getByName(k)
            if obj ~= nil then
                StaticObject.destroy(obj)
            end
        end
        game_state["DestroyedStatics"] = saved_game_state["DestroyedStatics"]
    end

    --Check all the ships
    local shipState = saved_game_state["Ships"]
    if shipState == nil then
        -- We probably didn't have this in the last state. Let's just build it.
        saved_game_state["Ships"] = {}
        shipState = saved_game_state["Ships"]
    end
    for _,ship in pairs(ShipGroupsTrackedByState) do
        -- If this exists, it means it was in the state.
        log("SHIPS -- Checking [" .. ship .. "] in game state.")
        local saved_ship = shipState[ship]
        if saved_ship then
            log("SHIPS -- ["..ship.."] exists.")
            if saved_ship["alive"] == false then
                log("SHIPS -- ["..ship.."] was not alive last state. Destroy it.")
                local shipGroup = Group.getByName(ship)
                if shipGroup then shipGroup:destroy() end
            end
        else
            log("SHIPS -- [".. ship .."] did not have an entry in the last state. Set it to alive.")
            shipState[ship] = {
                alive = true,
                groupName = ship
            }
        end
    end
    game_state["Ships"] = shipState


else
    -- Populate the world and gameplay environment.
    trigger.action.outText("No state file detected.  Creating new situation", 5)
    for i=1, 6 do
        local zone = getFreeZone(getSA6Zone)
        RussianTheaterSA6Spawn[1]:SpawnInZone(zone)
    end

    for i=1, 7 do
        local zone = nil
        if i < 4 then
            zone = getFreeZone(getSA10Zone)
            RussianTheaterSA10Spawn[1]:SpawnInZone(zone)
        end

        zone = getFreeZone(getSA10Zone)
        RussianTheaterEWRSpawn[1]:SpawnInZone(zone)

        zone = getFreeZone(getSA10Zone)
        RussianTheaterC2Spawn[1]:SpawnInZone(zone)
    end

    for i=1, 10 do
        local zone_index = math.random(18)
        local zone = "NorthStatic" .. zone_index
        local StaticSpawns = {AmmoDumpSpawn, PowerPlantSpawn, CommsArraySpawn}
        local spawn_index = math.random(3)
        local vec2 = mist.getRandomPointInZone(zone)
        local id = StaticSpawns[spawn_index]:Spawn({vec2.x, vec2.y})
    end

    for i=1,5 do
        local pt = get_oil_rig_location()
        local spawns = SpawnOilRigGroup(pt)
        AddNavalStrikeObjective(getMarkerId(), "Oil Rig #"..i, getCallsign(), spawns)
    end

    for gidx, group in ipairs(coalition.getGroups(1, 2)) do
        local groupname = Group.getName(group)
        if string.match(groupname, "AP DEF") then
            local spawned_group = Spawner(groupname):Spawn()
            game_state["Airfield Defenders"][groupname] = spawned_group:getName()
        end
    end


    --Initialize all the ships.
    game_state["Ships"] = {}
    local shipsState = game_state["Ships"]
    for _, ship in pairs(ShipGroupsTrackedByState) do
        log("SHIPS -- Initializing state for ship ["..ship.."]")
        shipsState[ship] = {
            alive = true,
            groupName = ship
        }
    end
end
log("Creating last airbase state")
mist.scheduleFunction(updateLastAirbaseState, {}, timer.getTime() + 3)

-- Kick off supports
mist.scheduleFunction(function()
    -- Friendly
    TexacoSpawn:Spawn()
    ShellSpawn:Spawn()
    OverlordSpawn:Spawn()
    CagAWACSSpawn:Spawn()
    ArcoSpawn:Spawn()
    -- Enemy
    RussianTheaterAWACSSpawn:Spawn()
end, {}, timer.getTime() + 30)

mist.scheduleFunction(function()
  RussianTheaterCASSpawn:Spawn()
  RussianTheaterCASSpawnF14:Spawn()
--   RussianTheaterCASSU24Spawn:Spawn()
  log("Spawned CAS Groups...")
end, {}, timer.getTime() + 1800, 1280)
-- Kick off the commanders
mist.scheduleFunction(russian_commander, {}, timer.getTime() + 1, 1000) -- CHANGE ME BACK

-- Check base ownership
mist.scheduleFunction(processLogiSlots, {}, timer.getTime() + 10, 60)

log("init.lua complete")
